// полифилл для получения свойства name у функции в старых браузерах < IE11
if ('name' in Function.prototype === false) {
	Object.defineProperty(Function.prototype, 'name', {
		get: function () {
			var matches = this.toString().match(/^\s*function\s*(\S[^\(]*)\s*\(/);
			var name = matches && matches.length > 1 ? matches[1] : '';
			Object.defineProperty(this, 'name', {value: name});
			return name;
		}
	});
}
if (window.jQuery) {
	const View = new (function () {
		// внутренние переменные
		const body = $(document.body);
		const htmlbody = $([document.documentElement, document.body]);
		const win = $(window);
		const doc = $(document)

		// внешние переменные
		// проверка на мобильное устройство (true/false)
		this.checkAdaptiveBrowser = (function () {
			let check = false;
			(function (a) {
				if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
			})(navigator.userAgent || navigator.vendor || window.opera);
			return check;
		})();
		this.isIE = (function detectIE() {
			var ua = window.navigator.userAgent;

			var msie = ua.indexOf('MSIE ');
			if (msie > 0) return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);

			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				var rv = ua.indexOf('rv:');
				return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
			}

			var edge = ua.indexOf('Edge/');
			if (edge > 0) return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);

			return false;
		})();
		this.isIOS = parseInt(('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0, ''])[1]).replace('undefined', '3_2').replace('_', '.').replace('_', '')) || false;

		this.screenSizeIsAdaptive = null;
		// проверка устройства десктоп или адатив
		this.mobileAndTabletCheck = null;
		// проверка наличия скроллбара у окна браузера
		this.winHaveScroll = null;
		// ширина скроллбара у окна браузера
		this.scrollBarWidth = null;
		// длительность анимации эффектов переключения всех переключающихся блоков
		this.animateDuration = 200
		// breakpoints
		this.breakpoints = {
			'lgMax': 1400,
			'lgMin': 1367,
			'mdMax': 1366,
			'mdMin': 1025,
			'smMax': 1024,
			'smMin': 768,
			'xsMax': 767,
			'xsMin': 320,
		}
		// спиннер при ожидании загрузки контента
		this.imageLoaderBase64 = 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNTBweCIgaGVpZ2h0PSI1MHB4IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxMDAgMTAwIiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWlkWU1pZCIgY2xhc3M9Imxkcy1kb3VibGUtcmluZyIgc3R5bGU9ImFuaW1hdGlvbi1wbGF5LXN0YXRlOiBydW5uaW5nOyBhbmltYXRpb24tZGVsYXk6IDBzOyBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDApIG5vbmUgcmVwZWF0IHNjcm9sbCAwJSAwJTsiPjxjaXJjbGUgY3g9IjUwIiBjeT0iNTAiIG5nLWF0dHItcj0ie3tjb25maWcucmFkaXVzfX0iIG5nLWF0dHItc3Ryb2tlLXdpZHRoPSJ7e2NvbmZpZy53aWR0aH19IiBuZy1hdHRyLXN0cm9rZT0ie3tjb25maWcuYzF9fSIgbmctYXR0ci1zdHJva2UtZGFzaGFycmF5PSJ7e2NvbmZpZy5kYXNoYXJyYXl9fSIgZmlsbD0ibm9uZSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiByPSI0MCIgc3Ryb2tlLXdpZHRoPSI2IiBzdHJva2U9IiM5M2RiZTkiIHN0cm9rZS1kYXNoYXJyYXk9IjYyLjgzMTg1MzA3MTc5NTg2IDYyLjgzMTg1MzA3MTc5NTg2IiBzdHlsZT0iYW5pbWF0aW9uLXBsYXktc3RhdGU6IHJ1bm5pbmc7IGFuaW1hdGlvbi1kZWxheTogMHM7Ij48YW5pbWF0ZVRyYW5zZm9ybSBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iIHR5cGU9InJvdGF0ZSIgY2FsY01vZGU9ImxpbmVhciIgdmFsdWVzPSIwIDUwIDUwOzM2MCA1MCA1MCIga2V5VGltZXM9IjA7MSIgZHVyPSIxcyIgYmVnaW49IjBzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgc3R5bGU9ImFuaW1hdGlvbi1wbGF5LXN0YXRlOiBydW5uaW5nOyBhbmltYXRpb24tZGVsYXk6IDBzOyI+PC9hbmltYXRlVHJhbnNmb3JtPjwvY2lyY2xlPjxjaXJjbGUgY3g9IjUwIiBjeT0iNTAiIG5nLWF0dHItcj0ie3tjb25maWcucmFkaXVzMn19IiBuZy1hdHRyLXN0cm9rZS13aWR0aD0ie3tjb25maWcud2lkdGh9fSIgbmctYXR0ci1zdHJva2U9Int7Y29uZmlnLmMyfX0iIG5nLWF0dHItc3Ryb2tlLWRhc2hhcnJheT0ie3tjb25maWcuZGFzaGFycmF5Mn19IiBuZy1hdHRyLXN0cm9rZS1kYXNob2Zmc2V0PSJ7e2NvbmZpZy5kYXNob2Zmc2V0Mn19IiBmaWxsPSJub25lIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHI9IjMzIiBzdHJva2Utd2lkdGg9IjYiIHN0cm9rZT0iIzY4OWNjNSIgc3Ryb2tlLWRhc2hhcnJheT0iNTEuODM2Mjc4Nzg0MjMxNTkgNTEuODM2Mjc4Nzg0MjMxNTkiIHN0cm9rZS1kYXNob2Zmc2V0PSI1MS44MzYyNzg3ODQyMzE1OSIgc3R5bGU9ImFuaW1hdGlvbi1wbGF5LXN0YXRlOiBydW5uaW5nOyBhbmltYXRpb24tZGVsYXk6IDBzOyI+PGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIGNhbGNNb2RlPSJsaW5lYXIiIHZhbHVlcz0iMCA1MCA1MDstMzYwIDUwIDUwIiBrZXlUaW1lcz0iMDsxIiBkdXI9IjFzIiBiZWdpbj0iMHMiIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIiBzdHlsZT0iYW5pbWF0aW9uLXBsYXktc3RhdGU6IHJ1bm5pbmc7IGFuaW1hdGlvbi1kZWxheTogMHM7Ij48L2FuaW1hdGVUcmFuc2Zvcm0+PC9jaXJjbGU+PC9zdmc+'
		this.setCookie = function (name, value, options) {
			options = options || {};

			var expires = options.expires;

			if (typeof expires == 'number' && expires) {
				var d = new Date();
				d.setTime(d.getTime() + expires * 1000);
				expires = options.expires = d;
			}
			if (expires && expires.toUTCString) {
				options.expires = expires.toUTCString();
			}

			value = encodeURIComponent(value);

			var updatedCookie = name + '=' + value;

			for (var propName in options) {
				updatedCookie += '; ' + propName;
				var propValue = options[propName];
				if (propValue !== true) {
					updatedCookie += '=' + propValue;
				}
			}

			document.cookie = updatedCookie;
		};
		// добавление на страницу отложенной загрузки внешнего скрипта
		this.loadDelayScript = function (url) {
			const script = $(document.createElement('script'))
			script.attr({src: url, type: 'text/javascript'})
			body.append(script)
		}
		// проверка расстояние до видимости блока больше или меншье указнного (при скролле страницы)
		this.getVisibleElemOnScroll = (elemJQ, reserveHeight) => {
			if (elemJQ.length > 0) {
				return elemJQ.offset().top - (doc.scrollTop() + win.height() + reserveHeight) < 0
			}
			return false
		}
		// ajax подгрузка при наличии параметра data-ajax с адресом файла блоков, параметры - (url, контейнер для выгрузки данных, контейнер локальной переинициализации, callback)
		this.ajaxLoad = function (ajaxUrl, ajaxContainerForData, containerForReInit, callback) {
			if (!ajaxContainerForData.prop('ajaxLoaded')) {
				$.get(ajaxUrl, function (data, status) {
					ajaxContainerForData.append(data)
					if (containerForReInit) View.initAllLocal(containerForReInit)
					if (status === 'success') {
						ajaxContainerForData.prop('ajaxLoaded', true)
						if (typeof callback === 'function') callback()
					}
				})
			} else {
				if (typeof callback === 'function') callback()
			}
		}
		// Видимости элемента после указанной высоты скролла страницы
		this.showElemAfterScroll = function (element, toggleHeightScroll) {
			element = $(element)
			win.off('scroll.showElemAfterScroll').on('scroll.showElemAfterScroll', toggleShow)
			win.trigger('scroll.showElemAfterScroll')

			function toggleShow() {
				var scrollDocument = win.scrollTop();

				if (scrollDocument < toggleHeightScroll) {
					element.fadeOut(300)
				} else {
					element.fadeIn(300)
				}
			}
		}

		// инициализация основных параметров класса View
		this.initView = function () {
			this.scrollBarWidth = (function () {
				const outer = document.createElement('div');
				outer.style.visibility = 'hidden';
				outer.style.width = '100px';
				outer.style.msOverflowStyle = 'scrollbar';

				document.body.appendChild(outer);
				const widthNoScroll = outer.offsetWidth;
				// force scrollbars
				outer.style.overflow = 'scroll';
				// add innerdiv
				const inner = document.createElement('div');
				inner.style.width = '100%';
				outer.appendChild(inner);
				const widthWithScroll = inner.offsetWidth;
				// remove divs
				outer.parentNode.removeChild(outer);
				return widthNoScroll - widthWithScroll;
			})();
			(function haveScroll() {
				let timeout = null

				function winHaveScroll() {
					View.winHaveScroll = win.height() < doc.height()
				}

				win.off('resize.winHaveScroll').on('resize.winHaveScroll', function () {
					clearTimeout(timeout)
					timeout = setTimeout(winHaveScroll, 300)
				})
				win.trigger('resize.winHaveScroll');
			})();
			(function screenWidthIsAdaptive() {
				let timeout = null

				function getScreenWidth() {
					if (View.winHaveScroll) {
						View.screenSizeIsAdaptive = (win.width() + View.scrollBarWidth) <= 1024
					} else {
						View.screenSizeIsAdaptive = win.width() <= 1024
					}
				}

				win.off('resize.getScreenWidth').on('resize.getScreenWidth', function () {
					clearTimeout(timeout)
					timeout = setTimeout(getScreenWidth, 300)
				})
				getScreenWidth()
			})();
			(function setMobileAndTabletCheck() {
				let timeout = null

				function setAdaptiveCheck() {
					View.mobileAndTabletCheck = View.checkAdaptiveBrowser || View.screenSizeIsAdaptive
				}

				win.off('resize.setAdaptiveCheck').on('resize.setAdaptiveCheck', function () {
					clearTimeout(timeout)
					timeout = setTimeout(setAdaptiveCheck, 300)
				})
				setAdaptiveCheck()
			})();
		}
		// управление открытием/закрытием всех переключающихся блоков и состояний
		this.control = {
			closeAllDropdowns() {
				const allParents = $('.js-dropdown')
				const allContents = allParents.find('.js-dropdown__content')
				allParents.removeClass('active')
				allContents.hide()
			},
			closeAllFocusBlocks() {
				$('.js-class-focus').removeClass('focus')
			},
			closeAllSingleSelect() {
				const allParents = $('.js-dropdown--select')
				const allContents = allParents.find('.js-dropdown__content')
				if (View.mobileAndTabletCheck) {
					allParents.removeClass('active  open')
					allContents.fadeOut(0)
				}
			},
			closeVariablesSearch() {
				$('.js-search__wrapper').fadeOut(0)
				$('.js-search__content').fadeOut(0)
			},
			closeAllFadeBlocks() {
				const allParents = $('.js-fade')
				const allContents = allParents.find('.js-fade__content')
				allParents.removeClass('active-fade')
				allContents.fadeOut(0)
			},
			closeAllPopups() {
				const allParents = $('.js-popup')
				const allContents = allParents.find('.js-popup__content')
				allParents.removeClass('active-fade')
				allContents.fadeOut(0)
			},
			closeAllClassBlocks() {
				const allParents = $('.js-class').removeClass('active-class')
				allParents.find('.js-class__target').removeClass('active-class scroll-disabled')
			},
			closeAllAccordions() {
				const allParents = $('.js-accordion').removeClass('open')
				allParents.find('.js-accordion__content').slideUp(View.animateDuration)
			},
			closeOverlayPage() {
				body.removeClass('overlay  fixed-scroll').css({'padding-right': 0})
				$('.js-overlay').removeClass('overlay').find('.js-overlay__content').hide()
				// кастомизации отдельных элементов
				$('.js-fixed').css({right: 0})
			},
			// закрыть всё что открывается, за исключением переданного списка параметров указанных имен закрывашек
			closeAll(...arrExcludeFuncName) {
				const arrFunctions = [
					this.closeAllDropdowns,
					this.closeAllFadeBlocks,
					this.closeVariablesSearch,
					this.closeOverlayPage,
					this.closeAllSingleSelect,
					this.closeAllClassBlocks,
					this.closeAllAccordions,
				]
				// console.log(arrExcludeFuncName)
				const arrLog = []
				// перебор массива со всеми имеющимися функциями для управлени
				$.each(arrFunctions, function (index, fn) {
					// наполнение массива для отладки функциями и их состояниями запуска
					arrLog.push(arrExcludeFuncName.every((excludeFuncName) => fn.name !== excludeFuncName) + ' = ' + fn.name)

					// если в массиве полученных имен функций для исключения есть текущая функция из общего массива функций
					if (arrExcludeFuncName.every((excludeFuncName) => fn.name != excludeFuncName)) fn();
				});
				// console.log(arrLog)
				console.log('close all')
			},
		}

		// инициализация
		this.init = {
			// global - инициализируется 1 раз
			global: {
				// скролл к блоку по ссылке
				scrollToBlock() {
					var items = $('.js-scroll-to')
					var blocks = []
					items.each(function () {
						var _this = $(this)
						// ID блока до, которого скролить
						var block = $(_this.attr('href'))
						blocks.push({
							top: block.offset().top,
							bottom: block.offset().top + block.outerHeight(),
							item: block,
							selector: _this.attr('href'),
							toggle: _this
						})

						_this.off('click.scrollToBlock').on('click.scrollToBlock', function (event) {
							event.preventDefault()
							items.removeClass('main-nav__item--active')
							_this.addClass('main-nav__item--active')
							win.off('scroll.scrollToBlock')

							// величина недоезда блока до верха в атрибуте ссылки
							var offset = $(_this).data('offset') || 0
							if (block.length) {
								var positionBlock = block.offset().top - offset

								htmlbody.animate({scrollTop: positionBlock}, 400, function () {
									setTimeout(function () {
										win.off('scroll.scrollToBlock').on('scroll.scrollToBlock', setActiveItem)
									}, 100)
								})
							}
						})
					})

					blocks = blocks.filter(function (item, index) {
						return index === blocks.findIndex((item2) => item2.selector === item.selector)
					})

					win.off('scroll.scrollToBlock').on('scroll.scrollToBlock', setActiveItem)

					function setActiveItem() {
						var scrollTop = doc.scrollTop()
						blocks.forEach(function (item) {
							if ((scrollTop + 100) >= item.top && scrollTop <= item.bottom) {
								items.removeClass('main-nav__item--active')
								item.toggle.addClass('main-nav__item--active')
							}
						})
					}
				},
				// подскролл страницы в начала по клику на кнопку
				scrollTopNative() {
					var item = $('.js-scroll-top')

					View.showElemAfterScroll(item, window.outerHeight / 2)

					item.off('click.scrollTop').on('click.scrollTop', function (event) {

						event.preventDefault()

						function animate({timing, draw, duration}) {                      // Вспомогательная функция animate для создания анимации
							let start = performance.now();
							requestAnimationFrame(function animate(time) {
								let timeFraction = (time - start) / duration;                          // timeFraction изменяется от 0 до 1
								if (timeFraction > 1) timeFraction = 1;
								let progress = timing(timeFraction);                    // вычисление текущего состояния анимации
								draw(progress);                          // отрисовать её
								if (timeFraction < 1) {
									requestAnimationFrame(animate);
								}
							})
						}

						animate({
							duration: 1000,
							timing: function (timeFraction) {
								return timeFraction;
							},            // линейная функция значит, что анимация идёт с одной и той же скоростью
							draw: function (progress) {
								document.documentElement.scrollTop = document.documentElement.scrollTop - (progress * document.documentElement.scrollTop)
							}
						});
					})
				},
				// сворачивание больших текстов по кнопке
				clippedText() {
					$('.js-clipped').each(function (ind) {
						var parent = $(this)
						var content = parent.find('.js-clipped__content')
						var toggler = parent.find('.js-clipped__toggler')
						var timeout = null;
						var textInit = content.html()
						var widthInit = parent.data('width-init')
						// высота текста из атрибута или дефолтная, которую стоит сворачивать
						var clippedHeight = parent.data('clippedHeight') || 100
						// колличество символов для показа сокращенной версии
						var clippedSymbols = parent.data('clippedSymbols')

						function clippedText() {
							// сбросс высоты контента
							parent.removeClass('init-clipped')
							content.css({maxHeight: ''})

							// если указано ограничение сокращенной версии по символам то высоту берем по их размеру высоты
							if (clippedSymbols) {
								content.text(content.text().substr(0, clippedSymbols))
								clippedHeight = content.outerHeight()
								content.html(textInit)
							}

							// изначальная высота блока с текстом
							var initialHeightContent = content.outerHeight()

							// инициализация плагина, если длинна текста больше допустимой
							if (initialHeightContent > clippedHeight) {
								parent.addClass('init-clipped')
								close()

								// переключение сворачивания контента
								toggler.off('click.clippedText').on('click.clippedText', function () {
									if (parent.hasClass('open-clipped')) {
										close()
									} else {
										parent.addClass('open-clipped')
										content.css({maxHeight: initialHeightContent})
									}
								})
							} else {
								toggler.off('click.clippedText')
							}
						}

						// закрыть выпадашку
						function close() {
							parent.removeClass('open-clipped')
							content.css({maxHeight: clippedHeight})
						}

						// закрыть выпадашку
						function open() {
							parent.removeClass('init-clipped')
							content.css({maxHeight: 'none'})
						}

						win.off('resize.clippedText' + ind).on('resize.clippedText' + ind, function () {
							if (win.width() <= widthInit) {
								clearTimeout(timeout);
								timeout = setTimeout(clippedText, 300);
							} else {
								open()
							}
						})

						win.trigger('resize.clippedText' + ind);
					})
				},
				// проверка скролла на начальное и конечное положение
				scrollShadowEdgeVanilla() {
					document.querySelectorAll('.js-scroll-shadow-edge').forEach(function (parent) {
						var content = parent.querySelector('.js-scroll-shadow-edge__content');
						var pos = parent.getAttribute('data-position');
						var prop = (pos === 'Left') ? 'Width' : 'Height';
						var endClassName = (pos.toLowerCase()) + '-end';
						var beginClassName = (pos.toLowerCase()) + '-begin';

						function toggleClass() {
							if ((content['scroll' + prop] - content['client' + prop]) >= 0) {
								parent.classList.toggle(endClassName, content['scroll' + pos] >= (content['scroll' + prop] - content['client' + prop]));
								parent.classList.toggle(beginClassName, content['scroll' + pos] <= 0);
							} else {
								parent.classList.add(endClassName + ' ' + beginClassName);
							}
						}

						toggleClass();

						content.removeEventListener('scroll', toggleClass);
						content.addEventListener('scroll', toggleClass);
						window.removeEventListener('resize', toggleClass);
						window.addEventListener('resize', toggleClass);
					});
				},
				// accordion
				toggleAccordion() {
					$('.js-accordion').each(function () {
						var parent = $(this)
						var content = parent.find('.js-accordion__content')

						// инициализация и закрытие всех аккордионов кроме тех что с классом open-accordion
						if (!parent.hasClass('open-accordion')) content.slideUp(0)
						parent.addClass('init-accordion')

						// переключение состояния аккордиона по клику
						parent.find('.js-accordion__toggler').off('click.toggleAccordion').on('click.toggleAccordion', function (event) {
							event.stopPropagation()
							event.preventDefault()

							parent.toggleClass('open-accordion')
							content.stop(true, true).slideToggle(300)
						})
					})
				},
				// переключение класса по клику
				toggleClassBlock() {
					var allParents = $('.js-class')

					allParents.each(function (ind) {
						var parent = $(this)
						// кастомный класс для переключения либо дефолтный
						var activeClass = parent.data('class') || 'active-class'

						parent.find('.js-class__toggler').off('click.toggleClass').on('click.toggleClass', function (event) {
							event.stopPropagation()
							event.preventDefault()

							if (!parent.hasClass(activeClass)) {
								parent.addClass(activeClass)
							} else {
								parent.removeClass(activeClass)
							}
						})

						doc.off('click.toggleClassBlock' + ind).on('click.toggleClassBlock' + ind, function (e) {
							if (!$(e.target).closest('.js-class').length) {
								parent.removeClass(activeClass)
							}
						})
					})
				},
				// фиксация блока при скролле страницы
				fixedBlockWhenScrolling() {
					$('.js-fixed').each(function (index) {
						var block = $(this)
						var content = block.find('.js-fixed__content')
						var blockHeight = content.outerHeight()
						var blockStart = block.offset().top
						var winWidth = win.width()

						block.addClass('init-fixed')
						block.css('padding-top', blockHeight)

						function fixedBlockWhenScrolling() {
							var docScroll = win.scrollTop()
							// фиксация блока
							if (!block.hasClass('active-fixed') && blockStart < docScroll) {
								doc.addClass('active-fixed')
								block.addClass('active-fixed').css('padding-top', blockHeight)
							} else if (block.hasClass('active-fixed') && blockStart >= docScroll) {
								// отмена фиксации блока
								doc.removeClass('active-fixed')
								block.removeClass('active-fixed')
							}
						}

						// инициализаци с индексом на случай множественных блоков для фиксации
						win.off('resize.fixedBlockWhenScrolling' + index)
							.on('resize.fixedBlockWhenScrolling' + index, function () {
								if (winWidth != win.width()) {
									winWidth = win.width()
									blockHeight = content.outerHeight()
									blockStart = block.offset().top
									block.css('padding-top', blockHeight)

									if (requestAnimationFrame) requestAnimationFrame(fixedBlockWhenScrolling)
									else fixedBlockWhenScrolling();
								}
							})

						// инициализаци с индексом на случай множественных блоков для фиксации
						win.off('scroll.fixedBlockWhenScrolling' + index)
							.on('scroll.fixedBlockWhenScrolling' + index, function () {
								if (requestAnimationFrame) requestAnimationFrame(fixedBlockWhenScrolling)
								else fixedBlockWhenScrolling();
							})
							.trigger('scroll.fixedBlockWhenScrolling' + index);
					})
				},
			},
			// local - требует переинициализации после перезагрузки DOM
			local: {},
		};
		this.initAllGlobal = function () {
			$.each(this.init.global, function (index, fn) {
				if (typeof fn === 'function') fn();
			});
		};
		this.initAllLocal = function (scope) {
			$.each(this.init.local, function (index, fn) {
				if (typeof fn === 'function') fn(scope);
			});
		};
	})()
	jQuery(document).ready(function ($) {
		const body = $(document.body);
		// body.removeClass('loading-page');

		body.addClass(View.isIOS ? ('ios ios-' + View.isIOS) : 'no-ios');
		body.addClass(View.checkAdaptiveBrowser ? 'touch' : 'no-touch');
		body.addClass(View.isIE ? 'ie' : 'no-ie');
		if (View.isIE) svg4everybody();

		View.initView();
		View.initAllGlobal();
		View.initAllLocal(body);

		// переинициализация функций посля аякс подгрузки блоков
		body.on('onAjaxReload', (e, scope) => {
			View.initAllLocal(scope);
		});
	})
}